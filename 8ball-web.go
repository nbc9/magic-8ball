package main

import (
    "fmt"
    "net/http"
    "math/rand"
    "time"
)


var magicAnswers = [...]string{
    // Positive outcomes
    "It is certain",
    "It is decidedly so",
    "Without a doubt",
    "Yes definitely",
    "You may rely on it",
    "As I see it, yes",
    "Most likely",
    "Outlook good",
    "Yes",
    "Signs point to yes",

    // Neutral outcomes
    "Reply hazy try again",
    "Ask again later",
    "Better not tell you now",
    "Cannot predict now",
    "Concentrate and ask again",

    // Negative outcomes
    "Don't count on it",
    "My reply is no",
    "My sources say no",
    "Outlook not so good",
    "Very doubtful",
}


func main() {
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        rand.Seed(time.Now().UnixNano())
        fmt.Fprintf(w, "Magic 8-Ball says: %s\n", magicAnswers[rand.Intn(len(magicAnswers))])
    })

    http.ListenAndServe(":8080", nil)
}
